
const routes = [
  { path: '/', component: () => import('pages/Index.vue') },
  { path: '/forgot', component: () => import('pages/ForgotPass.vue') },
  {
    path: '/home',
    component: () => import('layouts/Home.vue'),
    children: [
      { path: '', component: () => import('pages/Agenda.vue') },
      { path: 'report', component: () => import('pages/Report.vue') },
      { path: 'settings', component: () => import('pages/Settings.vue') },
      { path: 'help', component: () => import('pages/Help.vue') }
    ]
  }

]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
